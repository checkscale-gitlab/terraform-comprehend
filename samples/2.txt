Perth weather: Actual temperature higher than chilly Perth mornings feel
The West Australian
Thursday, 2 May 2019 10:23AM
Perth weather 8am
0:45 | Sunrise

Pause
Mute
Current Time
0:00
/
Duration
0:19

Fullscreen
Weather forecast for Perth Thursday 2/5/19

Autumn has well and truly set in and the last couple of days have felt exceptionally chilly, so it may surprise you to hear it’s not as cold as you think.

You may have always wondered what “feels like” or apparent temperature means.

A Bureau of Meteorology spokesperson said it was all down to wind and humidity.

“If we have a dry atmosphere or the dew point is quite low it tends to feel cooler, especially if it is combined with wind,” he said.

“But if the dew point is high — so it’s humid with a lack of wind — it tends to feel hotter than the actual temperature.”

Actual temperature is measured in a Stevenson or thermometer screen, taken in the shade and sheltered from elements such as wind chill, whereas apparent temperature isn’t so gives a better idea of thermal comfort felt.

This would explain the radical differences observed between actual and apparent differences at certain sites across the metropolitan area this morning

At 9.10am Gooseberry Hill measured 10.5C with an apparent temperature of 3.2C, whereas the Swan Valley’s Millendon’s was 13.2C with an apparent not far behind at 11.8C.

“The biggest difference there is wind. It’s quite calm at the Swan Valley site whereas in Gooseberry Hill there were wind speeds of 30kph, which highlights how wind contributes to a vastly different apparent temperature,” he said.

The chill we felt yesterday and this morning is normal for this time of the year; however, it looks like we’ll get a brief reprieve tomorrow with 28C degrees with a 9C minimum in the morning forecast for Perth.

But keep your umbrella handy as the cold front forecast for Saturday morning is expected to bring showers of 2-5ml around the metropolitan area followed by a chilly start to next week.