provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"

  backend "s3" {}
}

data "aws_caller_identity" "current" {}

locals {
  tags = "${merge(var.tags, map("Environment", "${var.environment}"))}"
}

module "stream_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"
  delimiter = "${var.delimiter}"

  attributes = [
    "stream",
  ]

  tags = "${local.tags}"
}

module "iam_policy_read_stream_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"
  delimiter = "${var.delimiter}"

  attributes = [
    "iam",
    "policy",
    "read",
    "stream",
  ]

  tags = "${local.tags}"
}

resource "aws_s3_bucket" "this" {
  bucket = "${module.stream_label.id}"
  acl    = "${var.s3_acl}"

  versioning {
    enabled = "${var.s3_versioning_enabled}"
  }

  # https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  # https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "${var.s3_sse_algorithm}"
      }
    }
  }

  lifecycle_rule {
    id      = "telemetry"
    enabled = true

    prefix = "telemetry/"

    tags = {
      "rule"      = "telemetry"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "ONEZONE_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }

    expiration {
      days = 120
    }
  }

  tags = "${module.stream_label.tags}"
}

module "stream_sentiment_result_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"
  delimiter = "${var.delimiter}"

  attributes = [
    "stream",
    "sentiment",
    "result",
  ]

  tags = "${local.tags}"
}

resource "aws_s3_bucket" "result" {
  bucket = "${module.stream_sentiment_result_label.id}"
  acl    = "${var.s3_acl}"

  versioning {
    enabled = "${var.s3_versioning_enabled}"
  }

  lifecycle_rule {
    id      = "result"
    enabled = true

    tags = {
      "rule"      = "result"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "ONEZONE_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }

    expiration {
      days = 120
    }
  }

  tags = "${module.stream_label.tags}"
}

resource "aws_kms_key" "this" {
  description             = "KMS key for Kinesis Stream"
  deletion_window_in_days = 30
}

resource "aws_kinesis_stream" "this" {
  name             = "${module.stream_label.id}"
  shard_count      = 1
  retention_period = 48

  encryption_type = "KMS"
  kms_key_id      = "${aws_kms_key.this.id}"

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = "${module.stream_label.tags}"
}

data "aws_iam_policy_document" "read_analysis_stream" {
  statement {
    effect = "Allow"

    actions = [
      "kinesis:ListStreams",
      "kinesis:SubscribeToShard",
      "kinesis:DescribeStreamSummary",
      "kinesis:ListShards",
      "kinesis:DescribeStreamConsumer",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords",
      "kinesis:DescribeStream",
      "kinesis:DescribeLimits",
      "kinesis:ListStreamConsumers",
      "kinesis:ListTagsForStream",
    ]

    resources = [
      "${aws_kinesis_stream.this.arn}",
      "${aws_kinesis_stream.this.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "read_analysis_stream_policy" {
  name        = "${module.iam_policy_read_stream_label.id}"
  description = "Policy allows reading Kinesis stream"
  policy      = "${data.aws_iam_policy_document.read_analysis_stream.json}"
}

module "iam_role_read_stream_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"
  delimiter = "${var.delimiter}"

  attributes = [
    "iam",
    "role",
    "read",
    "stream",
  ]

  tags = "${local.tags}"
}

module "iam_role_firehose_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"
  delimiter = "${var.delimiter}"

  attributes = [
    "iam",
    "role",
    "firehose",
  ]

  tags = "${local.tags}"
}

resource "aws_iam_role" "firehose" {
  name = "${module.iam_role_firehose_label.id}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "read_analysis_policy_attach" {
  role       = "${aws_iam_role.firehose.name}"
  policy_arn = "${aws_iam_policy.read_analysis_stream_policy.arn}"
}

resource "aws_kinesis_firehose_delivery_stream" "this" {
  name        = "${module.stream_label.id}"
  destination = "s3"

  kinesis_source_configuration {
    kinesis_stream_arn = "${aws_kinesis_stream.this.arn}"
    role_arn           = "${aws_iam_role.firehose.arn}"
  }

  s3_configuration {
    role_arn    = "${aws_iam_role.firehose.arn}"
    bucket_arn  = "${aws_s3_bucket.this.arn}"
    kms_key_arn = "${aws_kms_key.this.arn}"
  }
}

#
# IAM
#

data "aws_iam_policy_document" "read_put_telemetry" {
  statement {
    effect = "Allow"

    actions = [
      "s3:List*",
      "s3:Get*",
      "s3:Put*",
    ]

    resources = [
      "${aws_s3_bucket.this.arn}",
      "${aws_s3_bucket.this.arn}/*",
    ]
  }
}

module "iam_policy_read_put_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"

  attributes = [
    "read",
    "put",
    "iam",
    "policy",
  ]

  delimiter = "-"
  tags      = "${local.tags}"
}

resource "aws_iam_policy" "read_put_statements_policy" {
  name        = "${module.iam_policy_read_put_label.id}"
  description = "Policy allow reading and putting objects into stream bucket"

  policy = "${data.aws_iam_policy_document.read_put_telemetry.json}"
}

module "iam_role_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"

  attributes = [
    "iam",
    "assume",
    "role",
    "ec2",
  ]

  delimiter = "-"
  tags      = "${local.tags}"
}

data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = [
        "ec2.amazonaws.com",
      ]

      type = "Service"
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "role" {
  name = "${module.iam_role_label.id}"
  path = "/service/"

  assume_role_policy = "${data.aws_iam_policy_document.ec2_assume_role.json}"
}

resource "aws_iam_role_policy_attachment" "firehose" {
  role       = "${aws_iam_role.firehose.name}"
  policy_arn = "${aws_iam_policy.read_put_statements_policy.arn}"
}

resource "aws_iam_role_policy_attachment" "ec2" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "${aws_iam_policy.read_put_statements_policy.arn}"
}
